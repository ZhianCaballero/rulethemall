﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{

    public class AI : MonoBehaviour
    {
        private Animator animator;
        private Roomba roomba;
        public bool friendMode;

        public enum State
        {
            PATROL,
            JUMP,
            CHASE,
            ATTACK,
            STUNNED,
            CHASE_FRIEND_MODE,
            ATTACK_FRIEND_MODE,
            FOLLOW,
            PATROL_FRIEND_MODE,
            RETURN_OWNER,
            DIE
        }

        public State state;
        private bool alive;

        public GameObject[] waypoints;

        // For Not Friend mode

        // For patrolling
        public float patrolSpeed = 3f;
        public string waypointTag;
        private int waypointInd;
        private float distanceWaypoint;

        public GameObject target;

        // For Chasing
        public float chaseSpeed = 6f;
        public float chaseDistance = 10f;
        public float stopChaseDistance = 30f;

        // For Attack
        public float attackDistance = 5f;
        public float attackSpeed = 12f;


        // For Friend mode

        public GameObject owner;

        // Follow
        public float followSpeed = 12f;
        public float followAreaDistance = 30f;
        public float stopFollowDistance = 5f;

        // Patrolling
        public Vector3 patrolStart;
        public Vector3 patrolFinish;


        // Use this for initialization
        void Start()
        {
            roomba = GetComponent<Roomba>();
            animator = GetComponent<Animator>();

            waypoints = GameObject.FindGameObjectsWithTag(waypointTag);
            waypointInd = Random.Range(0, waypoints.Length - 1);
            if (!friendMode)
            {
                state = AI.State.PATROL;
            }
            else
            {
                state = AI.State.FOLLOW;
            }
            
            alive = true;
        }

        private GameObject[] getPlayers()
        {
            return GameObject.FindGameObjectsWithTag("Player");
        }

        private GameObject[] getEnemies()
        {
            return GameObject.FindGameObjectsWithTag("Enemy");
        }

        void Update()
        {
            if (alive)
            {
                if (!friendMode)
                {
                    this.gameObject.GetComponent<Renderer>().material.color = Color.red;
                    switch (state)
                    {
                        case State.PATROL:
                            Patrol();
                            break;
                        case State.JUMP:
                            Chase();
                            break;
                        case State.STUNNED:
                            Stunned();
                            break;
                        case State.CHASE:
                            Chase();
                            break;
                        case State.ATTACK:
                            Attack();
                            break;
                        case State.DIE:
                            Die();
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    this.gameObject.GetComponent<Renderer>().material.color = Color.green;
                    switch (state)
                    {
                        case State.ATTACK_FRIEND_MODE:
                            AttackFriendMode();
                            break;
                        case State.CHASE_FRIEND_MODE:
                            ChaseFriendMode();
                            break;
                        case State.PATROL_FRIEND_MODE:
                            Patrol();
                            break;
                        case State.FOLLOW:
                            Follow();
                            break;
                        case State.RETURN_OWNER:
                            ReturnOwner();
                            break;
                        case State.DIE:
                            Die();
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        void Patrol()
        {
            roomba.SetSpeed(patrolSpeed);
            distanceWaypoint = Vector3.Distance(this.transform.position, waypoints[waypointInd].transform.position);
            if (distanceWaypoint >= 4 && !roomba.IsMoving())
            {
                roomba.SetDestination(waypoints[waypointInd].transform.position);
                roomba.Move();
            }
            if (distanceWaypoint < 4)
            {
                waypointInd = Random.Range(0, waypoints.Length);
                roomba.SetDestination(waypoints[waypointInd].transform.position);
            }
            GameObject player = playerAtDistance(this.transform.position, chaseDistance);
            if (player != null)
            {
                target = player;
                state = State.CHASE;
            }

        }

        void Jump()
        {
        }

        void Chase()
        {
            GameObject playerChase = playerAtDistance(this.transform.position, stopChaseDistance);
            GameObject playerAttack = playerAtDistance(this.transform.position, attackDistance);
            if (playerChase != null && playerAttack == null)
            {
                target = playerChase;
                roomba.SetSpeed(chaseSpeed);
                roomba.SetDestination(target.transform.position);
            }
            else if (playerChase != null && playerAttack != null)
            {
                target = playerAttack;
                state = State.ATTACK;
            }
            else if (playerChase == null && playerAttack == null)
            {
                state = State.PATROL;
                waypointInd = Random.Range(0, waypoints.Length);
                roomba.SetDestination(waypoints[waypointInd].transform.position);
            }
        }

        void Attack()
        {
            GameObject playerChase = playerAtDistance(this.transform.position, stopChaseDistance);
            GameObject playerAttack = playerAtDistance(this.transform.position, attackDistance);
            if (playerChase != null && playerAttack == null)
            {
                target = playerChase;
                state = State.CHASE;
            }
            else if (playerChase != null && playerAttack != null)
            {
                target = playerAttack;
                roomba.SetSpeed(attackSpeed);
                roomba.Fire();
                state = State.ATTACK;
            }
            else if (playerChase == null && playerAttack == null)
            {
                state = State.PATROL;
                waypointInd = Random.Range(0, waypoints.Length);
                roomba.SetDestination(waypoints[waypointInd].transform.position);
            }
        }

        void Die()
        {
            roomba.SetDestination(target.transform.position);
        }

        void PatrolFriendMode()
        {

        }

        void Stunned()
        {
            roomba.Stop();
        }

        void AttackFriendMode()
        {
            GameObject playerAreaDistance = gameObjectAtDistanceGameObject(owner, this.transform.position, followAreaDistance);
            GameObject targetChase = gameObjectAtDistanceGameObject(target, this.transform.position, stopChaseDistance);
            GameObject targetAttack = gameObjectAtDistanceGameObject(target, this.transform.position, attackDistance);
            
            // Defend owner
            GameObject enemyAttakingOwner = enemyNearGameObject(owner, followAreaDistance);

            if (playerAreaDistance != null && targetChase != null && targetAttack == null)
            {
                target = targetChase;
                state = State.CHASE_FRIEND_MODE;
            }
            else if (playerAreaDistance != null && targetChase != null && targetAttack != null)
            {
                if (enemyAttakingOwner != null && !enemyAttakingOwner.Equals(targetAttack))
                {
                    state = State.RETURN_OWNER;
                }
                else
                {
                    target = targetAttack;
                    roomba.SetSpeed(attackSpeed);
                    roomba.Fire();
                    state = State.ATTACK_FRIEND_MODE;
                }
            }
            else
            {
                state = State.FOLLOW;
            }

        }

        void ChaseFriendMode()
        {
            GameObject playerAreaDistance = gameObjectAtDistanceGameObject(owner, this.transform.position, followAreaDistance);
            GameObject targetChase = gameObjectAtDistanceGameObject(target, this.transform.position, stopChaseDistance);
            GameObject targetAttack = gameObjectAtDistanceGameObject(target, this.transform.position, attackDistance);

            // Defend owner
            GameObject enemyAttakingOwner = enemyNearGameObject(owner, followAreaDistance);

            if (playerAreaDistance != null && targetChase != null && targetAttack == null)
            {
                target = targetChase;
                roomba.SetSpeed(chaseSpeed);
                roomba.SetDestination(target.transform.position);
            }
            else if (playerAreaDistance != null && targetChase != null && targetAttack != null)
            {
                if (enemyAttakingOwner != null && !enemyAttakingOwner.Equals(targetAttack))
                {
                    state = State.RETURN_OWNER;
                }
                else
                {
                    target = targetAttack;
                    state = State.ATTACK_FRIEND_MODE;
                }
            }
            else
            {
                state = State.FOLLOW;
            }
        }

        void Follow()
        {
            GameObject playerStopFollow = gameObjectAtDistanceGameObject(owner, this.transform.position, stopFollowDistance);
            GameObject playerAreaDistance = gameObjectAtDistanceGameObject(owner, this.transform.position, followAreaDistance);
            GameObject enemyToChase = enemyNearGameObject(this.gameObject, followAreaDistance);

            if(playerAreaDistance != null && enemyToChase != null)
            {
                target = enemyToChase;
                state = State.CHASE_FRIEND_MODE;
            }
            else if (playerStopFollow == null || playerAreaDistance == null)
            {
                roomba.SetSpeed(followSpeed);
                roomba.SetDestination(owner.transform.position);
                roomba.Move();
            }
            else if (playerStopFollow != null)
            {
                roomba.Stop();
            }
        }

        public void ReturnOwner()
        {

            GameObject playerStopFollow = gameObjectAtDistanceGameObject(owner, this.transform.position, stopFollowDistance);

            if (playerStopFollow == null)
            {
                roomba.SetSpeed(followSpeed);
                roomba.SetDestination(owner.transform.position);
                roomba.Move();
            }
            else
            {
                state = State.FOLLOW;
            }
        }

        public void setOwner(GameObject o)
        {
            owner = o;
        }

        void OnTriggerEnter(Collider coll)
        {
            //if (coll.tag == "Player")
            //{
            //    state = AI.State.CHASE;
            //    target = coll.gameObject;
            //}
            //else
            //{
            //    //state = AI.State.JUMP;
            //}
        }

        public bool isFriend(GameObject o)
        {
            return owner != null && owner.Equals(o);
        }

        public GameObject playerAtDistance(Vector3 position, float distance)
        {
            GameObject o = null;
            float minDistance = 0;
            foreach (GameObject player in getPlayers())
            {
                float enemyDistance = Vector3.Distance(position, player.transform.position);
                if (enemyDistance <= distance)
                {
                    if (owner == null || !owner.Equals(player))
                    {
                        if (minDistance == 0 || enemyDistance <= minDistance)
                        {
                            minDistance = enemyDistance;
                            o = player;
                        }
                    }
                }
            }
            return o;
        }

        public GameObject gameObjectAtDistanceGameObject(GameObject gameObject, Vector3 position, float distance)
        {
            GameObject o = null;
            if (gameObject != null && Vector3.Distance(position, gameObject.transform.position) <= distance)
            {
                o = gameObject;
            }
            return o;
        }

        public GameObject enemyNearGameObject(GameObject gameObject, float distance)
        {
            GameObject o = null;
            float minDistance = 0;
            foreach (GameObject enemy in getEnemies())
            {
                float enemyDistance = Vector3.Distance(gameObject.transform.position, enemy.transform.position);
                if (enemyDistance <= distance)
                {
                    AI roombaEnemie = enemy.GetComponent<AI>();
                    if (!roombaEnemie.isFriend(owner))
                    {
                        if (minDistance == 0 || enemyDistance <= minDistance)
                        {
                            minDistance = enemyDistance;
                            o = enemy;
                        }
                    }
                }
            }
            foreach (GameObject player in getPlayers())
            {
                float enemyDistance = Vector3.Distance(gameObject.transform.position, player.transform.position);
                if (enemyDistance <= distance)
                {
                    if (owner == null || !owner.Equals(player))
                    {
                        if (minDistance == 0 || enemyDistance <= minDistance)
                        {
                            minDistance = enemyDistance;
                            o = player;
                        }
                    }
                }
            }
            return o;
        }
    }
}

