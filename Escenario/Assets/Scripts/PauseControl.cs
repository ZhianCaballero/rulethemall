﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityStandardAssets.Characters.FirstPerson;

public class PauseControl : MonoBehaviour {

    public Button resume;
    public Button menu;
    public Canvas pause;
    public AudioClip buttonHoverSound;
    public AudioClip buttonClickedSound;

    public FirstPersonController player;

    private AudioSource audioSource;

    // Use this for initialization
    void Start () {
        pause.enabled = false;
        audioSource = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (pause.enabled == true)
            {
                PressResume();
                player.enabled = true;
            }
            else
            {
                pause.enabled = true;
                player.enabled = false;
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
	}

    public void PressResume()
    {
        Time.timeScale = 1;
        pause.enabled = false;
        player.enabled = true;
    }

    public void PressMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void PlayButtonHoverSound()
    {
        audioSource.PlayOneShot(buttonHoverSound);
    }

    public void PlayButtonClickSound()
    {
        audioSource.PlayOneShot(buttonClickedSound);
    }
}
