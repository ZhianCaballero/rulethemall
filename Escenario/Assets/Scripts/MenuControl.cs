﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour {

    public Canvas menu;
    public Canvas quitMenu;
    public Canvas multiplayerMenu;
    public Canvas highscoreMenu;
    public Canvas settingsMenu;
    public Button playBtn;
    public Button exitBtn;
    public AudioClip buttonHoverSound;
    public AudioClip buttonClickedSound;
    
    private AudioSource audioSource;
    public HighscoreControl highscoreControl;
    InputField nameField;

    // Use this for initialization
    void Start() {
        quitMenu.enabled = false;
        multiplayerMenu.enabled = false;
        highscoreMenu.enabled = false;
        settingsMenu.enabled = false;

        audioSource = GetComponent<AudioSource>();
        audioSource.Play();

        HighscoreControl.control.PlayerName = "Unknown player";

        nameField = (InputField)settingsMenu.GetComponentInChildren(typeof(InputField));
        nameField.text = HighscoreControl.control.PlayerName;        
    }

    // *** Main Menu ***
    public void PressSingleplayer()
    {
        audioSource.Stop();
        SceneManager.LoadScene(1);
    }

    public void PressMultiplayer()
    {
        menu.enabled = false;
        multiplayerMenu.enabled = true;
    }

    public void PressExit()
    {
        quitMenu.enabled = true;
        menu.enabled = false;
    }

    public void PressSettings()
    {
        menu.enabled = false;
        settingsMenu.enabled = true;

        nameField.text = HighscoreControl.control.PlayerName;
    }

    public void PressSettingsCancel()
    {
        settingsMenu.enabled = false;
        menu.enabled = true;
    }

    public void PressSettingsSave()
    {
        InputField nameField = (InputField)settingsMenu.GetComponentInChildren(typeof(InputField));
        if (!string.IsNullOrEmpty(nameField.text))
        {
            HighscoreControl.control.PlayerName = nameField.text;
            menu.enabled = true;
            settingsMenu.enabled = false;
        }
    }

    public void PressHighscore()
    {
        LoadHighscores();
        menu.enabled = false;
        highscoreMenu.enabled = true;
    }

    // *** Highscore Menu ***
    public void LoadHighscores()
    {
        highscoreControl.LoadFromDisk();
        string[] highscore = highscoreControl.FormatListText();

        if (highscore != null)
        {
            GameObject textFieldObj = GameObject.FindGameObjectWithTag("HighscoreText");
            Text textField = (Text)textFieldObj.GetComponent("Text");
            textField.text = highscore[0];

            GameObject textFieldObjValues = GameObject.FindGameObjectWithTag("HighscoreValues");
            Text textFieldValues = (Text)textFieldObjValues.GetComponent("Text");
            textFieldValues.text = highscore[1];
        }
        else {
            GameObject textFieldObj = GameObject.FindGameObjectWithTag("HighscoreText");
            Text textField = (Text)textFieldObj.GetComponent("Text");
            textField.text = "No data available";
        }
        
    }

    public void PressBackHighscore()
    {
        menu.enabled = true;
        highscoreMenu.enabled = false;
    }

    //*** Quit Menu ***

    public void PressYes()
    {
        Application.Quit();
    }

    public void PressNo()
    {
        quitMenu.enabled = false;
        menu.enabled = true;
    }

    //*** Sounds ***
    public void PlayButtonHoverSound()
    {
        audioSource.PlayOneShot(buttonHoverSound);
    }

    public void PlayButtonClickSound()
    {
        audioSource.PlayOneShot(buttonClickedSound);
    }
}
