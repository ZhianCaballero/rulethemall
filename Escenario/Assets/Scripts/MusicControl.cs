﻿using UnityEngine;
using System.Collections;

public class MusicControl : MonoBehaviour {

    
    private AudioSource audioSource;



    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();

        audioSource.playOnAwake = true;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void PlayMusic(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    public void PlayMusic()
    {
        audioSource.Play();
    }
}