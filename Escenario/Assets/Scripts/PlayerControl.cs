﻿using UnityEngine;
using System.Collections;
using System;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.Networking;

public class PlayerControl : MonoBehaviour
{    
    public GameObject bulletPrefab;
    public Transform bulletSpawn;    
    public AudioClip fireSound;

    private GameObject musicControlObj;
    private GameObject gameControlObj;
    private GameControl gameControl;
    private MusicControl musicControl;        
    private CharacterController m_CharacterController;

    // Use this for initialization
    void Start()
    {
        m_CharacterController = GetComponent<CharacterController>();
        musicControlObj = GameObject.Find("MusicControl");
        gameControlObj = GameObject.Find("GameControl");

        if (musicControlObj != null)
            musicControl = (MusicControl)musicControlObj.GetComponent("MusicControl");

        if (gameControlObj != null)
            gameControl = (GameControl)gameControlObj.GetComponent("GameControl");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {            
            Fire();
        }
        if(Input.GetMouseButtonDown(1))
        {
            RaycastToControl();
        }
    }

    private void RaycastToControl()
    {
        RaycastHit hit;
        Camera myCam = this.gameObject.GetComponentInChildren<Camera>();
        Ray vRay = myCam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0F));
        if (Physics.Raycast(vRay, out hit))
        {
            DrawLine(this.transform.position, hit.collider.transform.position, Color.blue);
            if (hit.collider.name != "" && hit.collider.tag == "Enemy")
            {
                AI roombaAi = hit.collider.gameObject.GetComponent<AI>();
                Roomba roomba = hit.collider.gameObject.GetComponent<Roomba>();
                if (roombaAi != null && !roombaAi.isFriend(this.gameObject) && roombaAi.state == AI.State.STUNNED)
                {
                    roombaAi.friendMode = true;
                    roombaAi.owner = this.gameObject;
                    roombaAi.state = AI.State.FOLLOW;
                    roomba.life = 200;
                    roomba.lifeBarObj.GetComponent<Renderer>().material.color = Color.green;
                    roomba.gameControl.DecreaseEnemyCount();
                    roomba.Move();
                }
            }
        }
    }

    private void Fire()
    {
        GameControl cotr = (GameControl)gameControl.GetComponent("Game Control");
        if (!gameControl.UIMode())
        {
            // Create the Bullet from the Bullet Prefab
            var bullet = (GameObject)Instantiate(
                bulletPrefab,
                bulletSpawn.position,
                bulletSpawn.rotation);


            BulletController bulletController = bullet.GetComponent<BulletController>();
            bulletController.player = this.gameObject;

            // Add velocity to the bullet
            bullet.GetComponent<Rigidbody>().velocity = m_CharacterController.velocity + bullet.transform.forward * 15;

            musicControl.PlayMusic(fireSound);

            // Destroy the bullet after 2 seconds
            Destroy(bullet, 2.0f);
        }
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "BulletRoombas")
        {
            gameControl.RestLife(0.1f);
        }
    }

    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.SetColors(color, color);
        lr.SetWidth(0.05f, 0.05f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(myLine, duration);
    }
}
