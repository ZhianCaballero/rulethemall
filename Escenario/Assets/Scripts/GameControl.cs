﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityStandardAssets.Characters.FirstPerson;

namespace UnityStandardAssets.Characters.ThirdPerson
{

    public class GameControl : MonoBehaviour
    {
        public Button resume;
        public Button menu;
        public Button newGame;
        public Button mainMenu;
        public Canvas pause;
        public Canvas gameOver;
        public AudioClip buttonHoverSound;
        public AudioClip buttonClickedSound;
        public Text resultText;
        public Text scoreText;
        public MusicControl musicControl;
        public FirstPersonController player;
        public HudControl hudControl;
        
                
        private int enemiesCount;
        float timer = 200;

        float spawnTime = 0;
        bool gameEnded = false;
        bool playerWin = false;

        // Use this for initialization
        void Start()
        {
            pause.enabled = false;
            gameOver.enabled = false;

            enemiesCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
            hudControl.SetRoombasCount(enemiesCount);

            HighscoreControl.control.LoadFromDisk();
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                if (pause.enabled == true)
                {
                    Resume();
                }
                else
                {
                    pause.enabled = true;
                    player.enabled = false;
                    PauseGame();
                }
            }

            timer -= Time.deltaTime;

            if (timer < 0)
            {
                ShowGameOver("TIME'S UP!", false);
            }

            hudControl.SetTime((int)timer);

            if (gameEnded)
            {
                spawnTime += Time.deltaTime;

                if (!playerWin && !UIMode()) {
                    ShowGameOver("YOU LOSE!", false);
                }
                if (spawnTime > 3 && playerWin )
                {
                    ShowGameOver("YOU WIN!", true);
                }
            }
        }

        public void Resume()
        {
            Time.timeScale = 1;
            pause.enabled = false;
            player.enabled = true;
            gameOver.enabled = false;
        }

        private void PauseGame()
        {
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        public void PressMenu()
        {
            Resume();
            SceneManager.LoadScene(0);
        }

        public void PressNewGame()
        {
            Resume();
            SceneManager.LoadScene(1);
        }

        public void PlayButtonHoverSound()
        {
            musicControl.PlayMusic(buttonHoverSound);
        }

        public void PlayButtonClickSound()
        {
            musicControl.PlayMusic(buttonClickedSound);
        }

        public bool UIMode()
        {
            return gameOver.enabled || pause.enabled;
        }

        private void ShowGameOver(string text, bool win)
        {
            player.enabled = false;            

            if (win)
            {                
                resultText.color = Color.green;
            }
            else
            {
                resultText.color = Color.red;
            }

            gameEnded = false;
            playerWin = false;
            gameOver.enabled = true;

            int score = hudControl.GetScore();
            resultText.text = text;
            scoreText.text += "\n" + score;

            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            LoadHighscores(score);
        }

        public void LoadHighscores(int score)
        {
            HighscoreControl.control.AddScore(score);
            HighscoreControl.control.SaveIntoDisk();

            string[] highscore = HighscoreControl.control.FormatListText();
            
            if (highscore != null)
            {
                GameObject textFieldObj = GameObject.FindGameObjectWithTag("HighscoreText");
                Text textField = (Text)textFieldObj.GetComponent("Text");
                textField.text = highscore[0];

                GameObject textFieldObjValues = GameObject.FindGameObjectWithTag("HighscoreValues");
                Text textFieldValues = (Text)textFieldObjValues.GetComponent("Text");
                textFieldValues.text = highscore[1];
            }            
        }

        public void DecreaseEnemyCount()
        {
            enemiesCount--;
            hudControl.SetRoombasCount(enemiesCount);
            hudControl.AddScore(50);
            //CheckGameState();
            if (enemiesCount == 0)
            {
                gameEnded = true;
                playerWin = true;
            }
        }

        public void AddScore(int value)
        {
            hudControl.AddScore(value);
        }

        public void RestLife(float value)
        {
            hudControl.RestLife(value);

            //CheckGameState();
            if (hudControl.IsDead())
            {
                //ShowGameOver("YOU LOSE!", false);
                playerWin = false;
                gameEnded = true;
            }
        }

        public void SetTime(int value)
        {
            hudControl.SetTime(value);
        }
    }
}