﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HudControl : MonoBehaviour {

    public Text time;
    public Text txtScore;
    public Text txtRoombaCount;
    public Slider healthBarSlider;

    private int score;
    private int roombasCount;

    
    // Use this for initialization
    void Start () {
        score = 0;
        roombasCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public int GetScore()
    {
        return score;
    }

    public void AddScore(int scoreValue)
    {
        score += scoreValue;
        txtScore.text = score.ToString();
    }

    public void SetRoombasCount(int value)
    {
        roombasCount = value;
        txtRoombaCount.text = roombasCount.ToString();
    }

    public void SetTime(int timeValue)
    {
        time.text = timeValue.ToString();
    }

    public void RestLife(float value)
    {
        if (healthBarSlider.value > 0)
        {
            healthBarSlider.value -= value;
        }        
    }

    public bool IsDead()
    {
        return healthBarSlider.value <= 0;
    }
}
