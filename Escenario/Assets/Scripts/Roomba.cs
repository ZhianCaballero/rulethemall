﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class Roomba : MonoBehaviour
    {
        public AI ai;

        public Vector3 destination;
        public GameObject explosion;
        public GameObject lifeBarObj;
        private NavMeshAgent agent;
        public float velocity;
        public float curSpeed;
        public float distanceDestiantion;
        public AudioClip explosionSound;
        public AudioClip shootSound;
        public GameControl gameControl;
        public MusicControl musicControl;

        private bool move = false;

        float[] values = { 0.0f, 0.5f, 1.0f, 1.5f, 2.0f, 2.5f, 3.0f, 3.5f, 4.0f };
        private bool collided;
        private Vector3 previousPosition;
        private LinkedList<TimePosition> positionsList;

        // Fire Stuff
        public GameObject bulletPrefab;
        public Transform bulletSpawn;

        [SerializeField]
        private float m_JumpSpeed;
        public int life = 100;
        private float lastFireTime = 0f;
        private float timer = 0;

        // Control fields
        private float lastWaitTime = 0f;
        private bool waiting = false;

        private class TimePosition
        {
            public Vector3 position;
            public float time;
            public float minDistance;

            public TimePosition(Vector3 p, float t, float md)
            {
                this.position = p;
                this.time = t;
                this.minDistance = md;
            }
        }


        public void SetDestination(Vector3 destination)
        {
            this.destination = destination;
            agent.destination = destination;
            agent.autoBraking = false;
            collided = false;
        }

        // Use this for initialization
        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            positionsList = new LinkedList<TimePosition>();
            lifeBarObj.GetComponent<Renderer>().material.color = Color.green;
            timer = 0;
        }

        void FixedUpdate()
        {
            float currentTime = Time.time;
            float deltaTime = currentTime - lastWaitTime;
            if (life < 20 && lastWaitTime == 0 && !ai.friendMode)
            {
                waiting = true;
                ai.state = AI.State.STUNNED;
                lastWaitTime = currentTime;
            }
            else if(deltaTime > 30 && waiting && !ai.friendMode)
            {
                waiting = false;
                ai.state = AI.State.PATROL;
            }
            if (!waiting)
            {
                timer = timer + Time.deltaTime;
                distanceDestiantion = Vector3.Distance(this.transform.position, destination);
                Vector3 curMove = transform.position - previousPosition;
                curSpeed = curMove.magnitude / Time.deltaTime;
                previousPosition = transform.position;

                TimePosition timePosition = null;
                if (positionsList.Count == 0)
                {
                    timePosition = new TimePosition(transform.position, timer, transform.position.magnitude);
                }
                else
                {
                    float currentDistance = Vector3.Distance(transform.position, positionsList.First.Value.position);
                    if (currentDistance < positionsList.First.Value.minDistance)
                    {
                        timePosition = new TimePosition(transform.position, timer, currentDistance);
                    }
                    else
                    {
                        timePosition = new TimePosition(transform.position, timer, positionsList.First.Value.minDistance);
                    }
                }
                positionsList.AddLast(timePosition);


                if ((collided && curSpeed < 1f) ||
                    (positionsList.Count > 30 && positionsList.Last.Value.minDistance < 0.2 &&
                    (positionsList.Last.Value.time - positionsList.First.Value.time) > 1))
                {
                    positionsList.Clear();
                    ResetPath();
                    collided = false;
                }
                if(life > 70)
                {
                    lifeBarObj.GetComponent<Renderer>().material.color = Color.green;
                }
                else if (life >= 30 && life <= 70)
                {
                    lifeBarObj.GetComponent<Renderer>().material.color = Color.yellow;
                }
                else if (life < 30)
                {
                    lifeBarObj.GetComponent<Renderer>().material.color = Color.red;
                }
            }
        }

        void OnCollisionEnter(Collision col)
        {
            if (col.collider.tag == "Enemy" || col.collider.tag == "Bullet"
                 || col.collider.tag == "Player" || col.collider.tag == "BulletRoomba")
            {
                collided = true;
                System.Random rnd = new System.Random();
                int index = rnd.Next(0, 8);
                StartCoroutine(WaitingMethod(values[index]));
            }
            if (col.collider.tag == "Bullet")
            {
                BulletController bulletController = col.collider.gameObject.GetComponent<BulletController>();
                GameObject attacker = bulletController.player;
                if (this.ai.owner == null || (!this.ai.owner.Equals(attacker)))
                {
                    DecrementLife();
                }
            }
            else if(col.collider.tag == "BulletRoomba")
            {
                BulletController bulletController = col.collider.gameObject.GetComponent<BulletController>();
                Roomba roombaKiller = bulletController.roomba;

                if(roombaKiller == null || (!roombaKiller.Equals(this) && roombaKiller.ai.owner != null && this.ai.owner != null && !roombaKiller.ai.owner.Equals(this.ai.owner))
                    || (!roombaKiller.Equals(this) && roombaKiller.ai.owner != null && this.ai.owner == null)
                    || (!roombaKiller.Equals(this) && roombaKiller.ai.owner == null && this.ai.owner != null))
                {
                    DecrementLife();
                }                
            }
        }

        private void DecrementLife()
        {
            gameControl.AddScore(10);
            if (life < 0)
            {
                // Destroy roomba
                Instantiate(explosion, transform.position, transform.rotation);
                musicControl.PlayMusic(explosionSound);

                Destroy(gameObject);

                gameControl.DecreaseEnemyCount();
            }
            else
            {
                life -= 30;
            }
        }

        IEnumerator WaitingMethod(float seconds)
        {
            if (agent.isActiveAndEnabled)
            {
                agent.Stop();
            }
            new WaitForSeconds(seconds);
            if (agent.isActiveAndEnabled)
            {
                agent.Resume();
            }
            yield return null;
        }

        public void ResetPath()
        {
            agent.ResetPath();
            agent.destination = this.destination;
        }

        public void Move()
        {
            if (agent.isActiveAndEnabled)
            {
                agent.Resume();
            }
        }

        public void Stop()
        {
            if (agent.isActiveAndEnabled)
            {
                agent.Stop();
            }
        }

        public void SetSpeed(float s)
        {
            agent.speed = s;
        }

        public bool IsMoving()
        {
            return curSpeed > 0;
        }

        public void Fire()
        {
            float currentTime = Time.time;
            float deltaTime = currentTime - lastFireTime;
            if (ai != null && (ai.state == AI.State.ATTACK || ai.state == AI.State.ATTACK_FRIEND_MODE) && ai.target != null && (lastFireTime == 0 || deltaTime > 0.25))
            {
                // Create the Bullet from the Bullet Prefab
                GameObject bullet = (GameObject)Instantiate(
                    bulletPrefab,
                    bulletSpawn.position,
                    bulletSpawn.rotation);

                BulletController bulletController = bullet.GetComponent<BulletController>();
                bulletController.roomba = this;

                musicControl.PlayMusic(shootSound);

                // Add velocity to the bullet
                Vector3 curMove = ai.target.transform.position - transform.position;
                bullet.GetComponent<Rigidbody>().velocity = (curMove.normalized * agent.speed) + bullet.transform.forward * 6;

                // Destroy the bullet after 2 seconds
                Destroy(bullet, 2.0f);
                lastFireTime = currentTime;
            }
        }
    }
}
