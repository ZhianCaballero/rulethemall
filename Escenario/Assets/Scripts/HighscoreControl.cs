﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System;
using System.Linq;

public class HighscoreControl : MonoBehaviour
{

    List<HighscoreData> scores;

    public static HighscoreControl control;

    public string PlayerName { get; set; }

    private void Awake()
    {
        if (control == null)
        {
            DontDestroyOnLoad(this.gameObject);
            control = this;
        }
        else if (control != this)
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        scores = new List<HighscoreData>();
        LoadFromDisk();
    }

    public void AddScore(string name, int score)
    {
        scores.Add(new HighscoreData(name, score));
    }

    public void AddScore(int score)
    {
        scores.Add(new HighscoreData(PlayerName, score));
    }

    public void SaveIntoDisk()
    {
        //File.Create(Application.persistentDataPath + "/highscore.txt");
        File.WriteAllText(Application.persistentDataPath + "/highscore.txt", string.Empty);
        List<string> lines = new List<string>();
        foreach (HighscoreData item in scores)
        {
            string line = item.name + "|" + item.score;
            lines.Add(line);
        }

        File.WriteAllLines(Application.persistentDataPath + "/highscore.txt", lines.ToArray());
    }

    public void LoadFromDisk()
    {
        scores = new List<HighscoreData>();

        if (File.Exists(Application.persistentDataPath + "/highscore.txt"))
        {
            using (var reader = new StreamReader(Application.persistentDataPath + "/highscore.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] values = line.Split('|');
                    string name = values[0].Trim();
                    int score = Convert.ToInt32(values[1].Trim());

                    AddScore(name, score);
                }
            }            
        }
    }

    public string[] FormatListText()
    {
        if (scores != null && scores.Count > 0)
        {
            StringBuilder strBiulder = new StringBuilder();
            StringBuilder strBiulderValues = new StringBuilder();

            List<HighscoreData> sortedList = scores.OrderByDescending(x => x.score).ToList();

            int count = 1;
            foreach (HighscoreData item in sortedList)
            {
                strBiulder.AppendLine(count + ".   " + item.name);
                strBiulderValues.AppendLine(item.score.ToString());
                count++;
            }
            string[] result = { strBiulder.ToString(), strBiulderValues.ToString() };
            return result;
        }
        else
        {
            return null;
        }
    }    

    public class HighscoreData
    {
        public int score;
        public string name;

        public HighscoreData(string name, int score)
        {
            this.score = score;
            this.name = name;
        }
    }
}
